git clone https://github.com/stumpwm/stumpwm.git

cd stumpwm && ./autogen.sh && ./configure && make && make install


DESKTOP_FILE=/usr/share/xsessions/stumpwm.desktop
if [-f "$DESKTOP_FILE"]; then
    echo "$DESKTOP_FILE Already exists"
else
    sudo cp stumpwm.desktop /usr/share/xsessions
fi

