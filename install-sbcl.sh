
if [ -x $(which sbcl) ] ; then
    echo "$(sbcl --version) found in the system."
else
    sudo apt install sbcl
fi
SBCL_PATH=$(which sbcl)

wget https://netix.dl.sourceforge.net/project/sbcl/sbcl/2.2.11/$1

tar -xvf $1


cd $2 && sh make.sh $SBCL_PATH && sh install.sh
