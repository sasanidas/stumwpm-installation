SBCL=sbcl-2.2.11-source.tar.bz2
SBCL_FOLDER=sbcl-2.2.11

clean-sbcl:
	rm ${SBCL} && rm -Rf ${SBCL_FOLDER}
clean-stumpwm:
	rm -Rf stumpwm

internal-install-sbcl:
	./install-sbcl.sh ${SBCL} ${SBCL_FOLDER}

internal-install-stumpwm:
	./install-stumpwm.sh

install-sbcl: internal-install-sbcl clean-sbcl

install-stumpwm: internal-install-stumpwm clean-stumpwm

install-all: install-sbcl install-stumpwm

